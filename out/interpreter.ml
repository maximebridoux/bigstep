module Input = struct
  type number = int
  type tunop = number -> number
  type tbinop = number * number -> number
  type state = number list
  type (_,_) term =
    | Nop : (state, state) term
    | Const : number -> (state, state) term
    | Cunop : tunop -> (state, state) term
    | Cbinop : tbinop -> (state, state) term
    | Cons : (state, state) term * (state, state) term -> (state, state) term
  and instructions = (state, state) term
  let print_value n = Printf.printf "value = %d\n" n
  let initial_state () = []
  let push s n = Some(n::s)
  let pop s = match s with
    | [] -> failwith "Empty"
    | (h::t) -> Some(t,h)
  let binop f n m = Some(f (n,m))
  let unop f n = Some(f n)
  let skip s = Some([])
  let id s = Some(s)
  let rec print_state s = match s with
    | [] -> ()
    | (h::t) -> Printf.printf "%d\n" h; print_state t;;
end

module InterpTest : (INTERPRETER with type number = int) = Out.MakeInterpreter(Input)

open InterpTest
open Script
open Print

let _ =
  let s = initial_state () in
    try
      while true do
        let lexbuf = Lexing.from_channel stdin in
        let result = Parser.script Lexer.token lexbuf in
        print_script result; print_newline; flush stdout
      done;

    with Script.Syntax(r, msg) -> print_int 3;;

  (* let plus x y = binop (fun (x,y) -> x+y) x y in *)
  (*print_state (eval s (Cons (Const 5,(Cons (Const 7, Const 9)))));;*)
