type ('number, 'tunop, 'tbinop, 'state, _, _) term = 
  | Cons : ('number, 'tunop, 'tbinop, 'state) instr * ('number, 'tunop, 'tbinop, 'state) instr_struct -> ('number, 'tunop, 'tbinop, 'state, 'state, 'state) term
  | Block : ('number, 'tunop, 'tbinop, 'state) instr_struct -> ('number, 'tunop, 'tbinop, 'state, 'state, 'state) term
  | Frame : ('number, 'tunop, 'tbinop, 'state) instr_struct * ('number, 'tunop, 'tbinop, 'state) frame_struct -> ('number, 'tunop, 'tbinop, 'state, 'state, 'state) term
  | Instr_end : ('number, 'tunop, 'tbinop, 'state, 'state, 'state) term
  | Frame_end : ('number, 'tunop, 'tbinop, 'state, 'state, 'state) term
  | Const : 'number -> ('number, 'tunop, 'tbinop, 'state, 'state, 'state) term
  | Cunop : 'tunop -> ('number, 'tunop, 'tbinop, 'state, 'state, 'state) term
  | Cbinop : 'tbinop -> ('number, 'tunop, 'tbinop, 'state, 'state, 'state) term
  | Nop : ('number, 'tunop, 'tbinop, 'state, 'state, 'state) term
  | Unreachable : ('number, 'tunop, 'tbinop, 'state, 'state, 'state) term
  | If : ('number, 'tunop, 'tbinop, 'state) instr_struct * ('number, 'tunop, 'tbinop, 'state) instr_struct -> ('number, 'tunop, 'tbinop, 'state, 'state, 'state) term
  | Br : 'number -> ('number, 'tunop, 'tbinop, 'state, 'state, 'state) term
  | Br_if : 'number -> ('number, 'tunop, 'tbinop, 'state, 'state, 'state) term
  | Return : ('number, 'tunop, 'tbinop, 'state, 'state, 'state) term
  | Call : 'number -> ('number, 'tunop, 'tbinop, 'state, 'state, 'state) term
  | Skip : ('number, 'tunop, 'tbinop, 'state, 'state, 'state) term
  | Loop : 'number * ('number, 'tunop, 'tbinop, 'state) instr_struct -> ('number, 'tunop, 'tbinop, 'state, 'state, 'state) term
  | Label : 'number * ('number, 'tunop, 'tbinop, 'state) instr_struct * ('number, 'tunop, 'tbinop, 'state) instr_struct -> ('number, 'tunop, 'tbinop, 'state, 'state, 'state) term
and ('number, 'tunop, 'tbinop, 'state) instr = ('number, 'tunop, 'tbinop, 'state, 'state, 'state) term
and ('number, 'tunop, 'tbinop, 'state) instr_struct = ('number, 'tunop, 'tbinop, 'state, 'state, 'state) term
and ('number, 'tunop, 'tbinop, 'state) frame_struct = ('number, 'tunop, 'tbinop, 'state, 'state, 'state) term
module type FLOW = sig
  type number
  type tunop
  type tbinop
  type state
  val initial_state : unit -> state
  val print_state : state -> unit
  val push : state -> number -> state option
  val pop : state -> (state * number) option
  val binop : tbinop -> number -> number -> number option
  val unop : tunop -> number -> number option
  val id : state -> state option
  val trap : state -> state option
  val isZero : number -> unit option
  val isnotZero : number -> unit option
  val isBranching : state -> unit option
  val isNotBranching : state -> unit option
  val makeBrState : state -> number -> state option
  val makeLabel : number -> (number, tunop, tbinop, state) instr_struct -> (number, tunop, tbinop, state) instr_struct -> (number, tunop, tbinop, state) instr option
  val makeBlankLabel : (number, tunop, tbinop, state) instr_struct -> (number, tunop, tbinop, state) instr option
  val makeLoop : number -> (number, tunop, tbinop, state) instr_struct -> (number, tunop, tbinop, state) instr_struct option
  val saveStack : state -> state option
  val isNotBr0 : state -> unit option
  val reduceBr : state -> state option
  val isBr0 : state -> unit option
  val popNValues : state -> number -> (state * state) option
  val restoreStack : state -> number -> state -> state option
  val pushNValues : state -> state -> state option
end

module type INTERPRETER = sig
  type number
  type tunop
  type tbinop
  type state
  val initial_state : unit -> state
  val print_state : state -> unit
  val eval : 'a -> (number, tunop, tbinop, state, 'a, 'b) term -> 'b
end

module MakeInterpreter (F : FLOW) = struct
  include F
  type f = { f : 'a 'b. 'a -> (number, tunop, tbinop, state, 'a, 'b) term -> 'b }
  let eval_cons {f} x_t1 x_t2 state = 
    let x_f = f state x_t1 in
    let x_o = 
      try 
        let () = 
          match isBranching x_f with
          | None -> failwith "isBranching"
          | Some result -> result
        in
        let x_o = 
          match id x_f with
          | None -> failwith "id"
          | Some result -> result
        in
        x_o
      with _ ->
      try 
        let () = 
          match isNotBranching x_f with
          | None -> failwith "isNotBranching"
          | Some result -> result
        in
        let x_o = f x_f x_t2 in
        x_o
      with _ ->
        failwith "All branches have failed"
    in
    x_o
  let eval_instr_end {f} state = 
    let x_o = 
      match id state with
      | None -> failwith "id"
      | Some result -> result
    in
    x_o
  let eval_frame_end {f} state = 
    let x_o = 
      match id state with
      | None -> failwith "id"
      | Some result -> result
    in
    x_o
  let eval_skip {f} state = 
    let x_o = 
      match id state with
      | None -> failwith "id"
      | Some result -> result
    in
    x_o
  let eval_const {f} x_t1 state = 
    let x_o = 
      match push state x_t1 with
      | None -> failwith "push"
      | Some result -> result
    in
    x_o
  let eval_unop {f} x_t1 state = 
    let x_f1, x_t2 = 
      match pop state with
      | None -> failwith "pop"
      | Some result -> result
    in
    let x_t3 = 
      match unop x_t1 x_t2 with
      | None -> failwith "unop"
      | Some result -> result
    in
    let x_o = 
      match push x_f1 x_t3 with
      | None -> failwith "push"
      | Some result -> result
    in
    x_o
  let eval_binop {f} x_t1 state = 
    let x_f1, x_t2 = 
      match pop state with
      | None -> failwith "pop"
      | Some result -> result
    in
    let x_f2, x_t3 = 
      match pop x_f1 with
      | None -> failwith "pop"
      | Some result -> result
    in
    let x_t4 = 
      match binop x_t1 x_t2 x_t3 with
      | None -> failwith "binop"
      | Some result -> result
    in
    let x_o = 
      match push x_f2 x_t4 with
      | None -> failwith "push"
      | Some result -> result
    in
    x_o
  let eval_nop {f} state = 
    let x_o = 
      match id state with
      | None -> failwith "id"
      | Some result -> result
    in
    x_o
  let eval_unreachable {f} state = 
    let x_o = 
      match trap state with
      | None -> failwith "trap"
      | Some result -> result
    in
    x_o
  let eval_block {f} x_t1 state = 
    let x_t2 = 
      match makeBlankLabel x_t1 with
      | None -> failwith "makeBlankLabel"
      | Some result -> result
    in
    let x_o = f state x_t2 in
    x_o
  let eval_loop {f} x_t1 x_t2 state = 
    let x_t3 = 
      match makeLoop x_t1 x_t2 with
      | None -> failwith "makeLoop"
      | Some result -> result
    in
    let x_t4 = 
      match makeLabel x_t1 x_t2 x_t3 with
      | None -> failwith "makeLabel"
      | Some result -> result
    in
    let x_o = f state x_t4 in
    x_o
  let eval_branch {f} x_t1 state = 
    let x_o = 
      match makeBrState state x_t1 with
      | None -> failwith "makeBrState"
      | Some result -> result
    in
    x_o
  let eval_if {f} x_t1 x_t2 state = 
    let x_f1, x_t3 = 
      match pop state with
      | None -> failwith "pop"
      | Some result -> result
    in
    let x_o = 
      try 
        let () = 
          match isZero x_t3 with
          | None -> failwith "isZero"
          | Some result -> result
        in
        let x_o = f x_f1 x_t1 in
        x_o
      with _ ->
      try 
        let () = 
          match isnotZero x_t3 with
          | None -> failwith "isnotZero"
          | Some result -> result
        in
        let x_o = f x_f1 x_t2 in
        x_o
      with _ ->
        failwith "All branches have failed"
    in
    x_o
  let eval_label {f} x_t1 x_t2 x_t3 state = 
    let x_f1 = 
      match saveStack state with
      | None -> failwith "saveStack"
      | Some result -> result
    in
    let x_f2 = f state x_t2 in
    let x_o = 
      try 
        let () = 
          match isNotBranching x_f2 with
          | None -> failwith "isNotBranching"
          | Some result -> result
        in
        let x_o = 
          match id x_f2 with
          | None -> failwith "id"
          | Some result -> result
        in
        x_o
      with _ ->
      try 
        let () = 
          match isBranching x_f2 with
          | None -> failwith "isBranching"
          | Some result -> result
        in
        let () = 
          match isNotBr0 x_f2 with
          | None -> failwith "isNotBr0"
          | Some result -> result
        in
        let x_o = 
          match reduceBr x_f2 with
          | None -> failwith "reduceBr"
          | Some result -> result
        in
        x_o
      with _ ->
      try 
        let () = 
          match isBranching x_f2 with
          | None -> failwith "isBranching"
          | Some result -> result
        in
        let () = 
          match isBr0 x_f2 with
          | None -> failwith "isBr0"
          | Some result -> result
        in
        let x_f4, x_f3 = 
          match popNValues x_f2 x_t1 with
          | None -> failwith "popNValues"
          | Some result -> result
        in
        let x_f5 = 
          match restoreStack x_f1 x_t1 x_f4 with
          | None -> failwith "restoreStack"
          | Some result -> result
        in
        let x_f6 = 
          match pushNValues x_f5 x_f3 with
          | None -> failwith "pushNValues"
          | Some result -> result
        in
        let x_o = f x_f6 x_t3 in
        x_o
      with _ ->
        failwith "All branches have failed"
    in
    x_o
  let rec eval : type a b. a -> (number, tunop, tbinop, state, a, b) term -> b =
    fun state term -> 
      match term with
      | Cons (x_t1, x_t2) -> eval_cons {f = eval} x_t1 x_t2 state
      | Instr_end -> eval_instr_end {f = eval} state
      | Frame_end -> eval_frame_end {f = eval} state
      | Skip -> eval_skip {f = eval} state
      | Const x_t1 -> eval_const {f = eval} x_t1 state
      | Cunop x_t1 -> eval_unop {f = eval} x_t1 state
      | Cbinop x_t1 -> eval_binop {f = eval} x_t1 state
      | Nop -> eval_nop {f = eval} state
      | Unreachable -> eval_unreachable {f = eval} state
      | Block x_t1 -> eval_block {f = eval} x_t1 state
      | Loop (x_t1, x_t2) -> eval_loop {f = eval} x_t1 x_t2 state
      | Br x_t1 -> eval_branch {f = eval} x_t1 state
      | If (x_t1, x_t2) -> eval_if {f = eval} x_t1 x_t2 state
      | Label (x_t1, x_t2, x_t3) -> eval_label {f = eval} x_t1 x_t2 x_t3 state
end
